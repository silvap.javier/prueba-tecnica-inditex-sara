module.exports = {
    webpack: {
      configure: (webpackConfig, { env }) => {
        if (env === 'development') {
            webpackConfig.optimization.minimize = false;
        } else {
          webpackConfig.optimization.minimize = true;
          webpackConfig.optimization.splitChunks = {
            chunks: 'all',
            cacheGroups: {
              default: false,
              vendors: false,
            }
          };
        }
        return webpackConfig;
      }
    }
  };