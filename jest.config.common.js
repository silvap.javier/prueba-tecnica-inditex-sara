import { paths } from 'react-app-rewired';
import { readdirSync } from 'fs';
import path from 'path';

const testPathIgnorePatterns = [
  '/node_modules/',
  '/build/',
  '/dist/',
  '/coverage/',
  '/src/test/*.tsx',
];

// Encuentra todas las carpetas bajo `src` y las convierte en rutas relativas.
const srcPath = path.resolve(__dirname, 'src');
const srcSubDirectories = readdirSync(srcPath, { withFileTypes: true })
  .filter((dirent) => dirent.isDirectory())
  .map((dirent) => path.join('src', dirent.name));

module.exports = {
  roots: srcSubDirectories,
  testMatch: ['**/__tests__/**/*.spec.+(ts|tsx|js|jsx)'],
  collectCoverageFrom: ['src/**/*.{ts,tsx,js,jsx}', '!src/**/*.d.ts','!src/test/utils/test.tsx'],
  coverageReporters: ['lcov', 'text'],
  coveragePathIgnorePatterns: ['/node_modules/', '/dist/', '/build/'],
  coverageThreshold: {
    global: {
      statements: 80,
      branches: 80,
      lines: 80,
      functions: 80,
    },
  },
  testPathIgnorePatterns,
  moduleDirectories: ['node_modules', 'src'],
  moduleNameMapper: {
    '\\.(css|less|scss|sass)$': 'identity-obj-proxy',
  },
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  setupFilesAfterEnv: [paths.testsSetup],
};
