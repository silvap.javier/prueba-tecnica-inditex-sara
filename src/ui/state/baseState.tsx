import React, { createContext, useState, ReactNode, useContext } from "react";
import { Result } from "../types/characters.types";

interface FavoriteLink extends Result {}

interface BaseStateContextProps {
    favorites: FavoriteLink[];
    addFavorite: (favorite: FavoriteLink) => void;
    removeFavorite: (id: number) => void;
    handleFavorite: (favorite: FavoriteLink) => void;
    getStatusFavorite: (id: number | undefined) => boolean;
    isLoading: boolean;
    setIsLoading: (value: boolean) => void;
}

const BaseStateContext = createContext<BaseStateContextProps | undefined>(undefined);

export const BaseStateProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
    const [favorites, setFavorites] = useState<FavoriteLink[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const addFavorite = (favorite: FavoriteLink) => {
        setFavorites((prevFavorites) => [...prevFavorites, favorite]);
    };

    const removeFavorite = (id: number) => {
        setFavorites((prevFavorites) => prevFavorites.filter((fav) => fav.id !== id));
    };

    const handleFavorite = (favorite: FavoriteLink) => {
        const isFavorite = favorites.some((fav) => fav.id === favorite.id);
        if (isFavorite) {
            removeFavorite(favorite.id);
        } else {
            addFavorite(favorite);
        }
    };

    const getStatusFavorite = (id: number | undefined) => {
        if (!id) return false;
        const favorite = favorites.some((fav) => fav.id === id);
        return favorite ? true : false;
    };

    return (
        <BaseStateContext.Provider
            value={{ favorites, addFavorite, removeFavorite, handleFavorite, getStatusFavorite, isLoading, setIsLoading }}
        >
            {children}
        </BaseStateContext.Provider>
    );
};

export const useBaseState = (): BaseStateContextProps => {
    const context = useContext(BaseStateContext);
    if (context === undefined) {
        throw new Error("useBaseState must be used within a BaseStateProvider");
    }
    return context;
};
