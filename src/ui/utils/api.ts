import md5 from 'md5';

export const getApi = async (url: string,limit : number) : Promise<any> => {
    const API_PUBLIC_KEY = process.env.REACT_APP_API_PUBLIC_KEY;
    const API_PRIVATE_KEY = process.env.REACT_APP_API_PRIVATE_KEY;
    const API_URL = process.env.REACT_APP_API_URL;
    const ts = new Date().getTime();
    const hash = md5(ts+API_PRIVATE_KEY!+API_PUBLIC_KEY)
    const response = await fetch(API_URL + `${url}?apikey=${API_PUBLIC_KEY}&ts=${ts}&hash=${hash}&limit=${limit}`);
    return response.json();
}