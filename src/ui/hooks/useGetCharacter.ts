import { useQuery } from '@tanstack/react-query';
import { getApi } from '../utils/api';
import { CharactersResponse } from '../types/characters.types';


const fetchCharacterById = async (id:number) => {
    return getApi('characters/'+id,1);
};

const useGetCharacter = (id:number) => {
    return  useQuery<CharactersResponse>({ queryKey: ['getCharacter',id], queryFn: () => fetchCharacterById(id) })
};

export default useGetCharacter;