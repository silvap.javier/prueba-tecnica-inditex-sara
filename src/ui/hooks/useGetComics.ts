import { useQuery } from '@tanstack/react-query';
import { getApi } from '../utils/api';
import { ComicsResponse } from '../types/comics.types';


const fetchComicsById = async (id:number) => {
    return getApi('characters/'+id+'/comics',20);
};

const useGetComics = (id:number) => {
    return  useQuery<ComicsResponse>({ queryKey: ['useGetComics',id], queryFn: () => fetchComicsById(id) })
};

export default useGetComics;