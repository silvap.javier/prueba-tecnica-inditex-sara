import { useQuery } from '@tanstack/react-query';
import { CharactersResponse } from '../types/characters.types';
import { getApi } from '../utils/api';

const fetchCharacters = async () => {
  return getApi('characters',50);
};

const useGetCharacters = () => {
  return  useQuery<CharactersResponse>({ queryKey: ['getCharacteres'], queryFn: fetchCharacters })
};

export default useGetCharacters;
