import { HeaderProps } from "./Header.types";
import clsx from "clsx";
import { HeaderLink, HeaderRoot, LoadingBar } from "./Header.styles";
import { MainRoutes } from "../../router/MainRouter.types";
import logo from "../../assets/images/logo.png";
import Favorites from "../../containers/Favorites/Favorites";
import { useBaseState } from "../../state/baseState";

function Header({ className, ...rest }: HeaderProps) {
    const { isLoading } = useBaseState();
    return (
        <HeaderRoot className={clsx("header", className)} {...rest}>
            {isLoading && <LoadingBar />}
            <HeaderLink to={MainRoutes.MAIN}>
                <img src={logo} alt="logo" />
            </HeaderLink>
            <Favorites />
        </HeaderRoot>
    );
}

export default Header;
