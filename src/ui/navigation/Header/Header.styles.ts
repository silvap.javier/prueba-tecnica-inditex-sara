
import { Link } from "react-router-dom";
import { css, styled } from "styled-components";
import Row from "../../design-system/Row/Row";

export const HeaderRoot = styled(Row)(
    ({ theme }) => css`
        justify-content: space-between;
        align-items: center;
        padding: ${theme.spacing["4"]} ${theme.spacing["12"]};
        background-color: ${theme.palette.black};
        border-bottom: 1px solid ${theme.palette.lightGray};
        margin-bottom: 0px;
        position: relative;
        z-index: 10; 

        a {
            text-decoration: none;
        }

        @media (max-width: 768px) {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            padding: ${theme.spacing["4"]} ${theme.spacing["4"]};
        }
    `,
);

export const HeaderLink = styled(Link)(
    () => css`
        text-decoration: none;
    `,
);

export const LoadingBar = styled.div`
    position: absolute;
    top: 88px;
    left: 0;
    width: 100%;
    height: 4px;
    background-color: ${(props) => props.theme.palette.marvelRed};
    animation: loading 2s infinite;

    @keyframes loading {
        0% {
            transform: translateX(-100%);
        }
        50% {
            transform: translateX(0);
        }
        100% {
            transform: translateX(100%);
        }
    }
`;