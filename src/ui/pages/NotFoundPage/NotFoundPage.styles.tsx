import styled, { css } from "styled-components";
import BaseLayout from "../../layout/BaseLayout/BaseLayout";
import Typography from "../../components/Typography/Typography";

export const NotFoundPageRoot = styled(BaseLayout)(
    () => css`
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        flex-direction: column;
        justify-content: center;
        row-gap: 5;
    `,
);

export const NotFoundText = styled(Typography)(
    ({ theme }) => css`
        color: ${theme.palette.primary};
        text-align: center;
    `,
);

export const NotFound404Text = styled(NotFoundText)(
    () => css`
        font-size: 11rem;
        line-height: unset;
    `,
);
