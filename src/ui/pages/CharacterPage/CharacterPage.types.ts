export interface CharacterPageProps {
    className?: string;
    style?: React.CSSProperties;
}