import styled, { css } from "styled-components";
import BasePage from "../../layout/BasePage/BasePage";
import Col from "../../design-system/Col/Col";
import Row from "../../design-system/Row/Row";


export const CharacterPageRoot = styled(BasePage)(
    () => css`
        padding: 0px;
    `,
);

export const CharacterPageWrapper = styled(Col)(
    ({ theme }) => css`
        row-gap: ${theme.spacing["7"]};
        flex: 1;
    `,
);

export const CharacterPageWrapperHeader = styled(Row)(
    ({ theme }) => css`
        position: relative;
        background-color: ${theme.palette.black};
        color: ${theme.palette.white};
        justify-content: center;

        .Content {
            padding: 0px ${theme.spacing["12"]};
            display: flex;
            flex-direction: column; /* Default to column layout */
        }

        .Description {
            line-height: 1.5;
        }

        &:after {
            content: '';
            position: absolute;
            bottom: 0;
            right: 0;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0 0 20px 20px;
            border-color: transparent transparent white transparent;
            z-index: 2;
        }

        @media (max-width: 768px) {
            width: 100%;

            img {
                width: 100%;
                height: auto;
            }

            .Content {
                width: 100%;
                padding: 0;
                max-width: 90%;
                flex: 100%;

                .Title {
                    flex-direction: row; /* Change to row layout in mobile */
                    align-items: center;
                    width: 100%;
                }
            }
        }
    `,
);

export const CharacterPageWrapperContent = styled(Col)(
    () => css`
        width: 100%;
        margin:auto;

        @media (max-width: 768px) {
            max-width: 90%;
        }
    `,
);
