import clsx from "clsx";
import { CharacterPageRoot, CharacterPageWrapper, CharacterPageWrapperContent, CharacterPageWrapperHeader } from "./CharacterPage.styles";
import { CharacterPageProps } from "./CharacterPage.types";
import { useParams } from "react-router-dom";
import useGetCharacter from "../../hooks/useGetCharacter";
import Typography from "../../components/Typography/Typography";
import { Result } from "../../types/characters.types";
import CharacterImg from "../../components/CharacterImg/CharacterImg";
import Col from "../../design-system/Col/Col";
import Row from "../../design-system/Row/Row";
import FavoriteIcon from "../../containers/FavoriteIcon/FavoriteIcon";
import Carousel from "../../design-system/Carousel/Carousel";
import useGetComics from "../../hooks/useGetComics";
import ComicItem from "../../containers/ComicItem/ComicItem";
import { useEffect } from "react";
import { useBaseState } from "../../state/baseState";
import useIsMobile from "../../hooks/useIsMobile";

function CharacterPage({ className, ...rest }: CharacterPageProps): JSX.Element {
    const params = useParams();
    const id = Number(params.id);
    const { data, isLoading: isLoadingCharacter } = useGetCharacter(id);
    const { data: comics, isLoading: isLoadingComic } = useGetComics(id);
    const { setIsLoading } = useBaseState();
    const isMobile = useIsMobile();
    const isLoading = isLoadingCharacter && isLoadingComic;
    const character = data?.data.results[0] as Result;
    const characterImg = character?.thumbnail ? `${character?.thumbnail.path}.${character?.thumbnail.extension}` : "";

    useEffect(() => {
        if (isLoading) {
            setIsLoading(true);
        } else {
            setIsLoading(false);
        }
    }, [isLoading, setIsLoading]);

    return (
        <CharacterPageRoot className={clsx("character-page", className)} {...rest}>
            <CharacterPageWrapper span={12}>
                <CharacterPageWrapperHeader>
                    <CharacterImg
                        url={characterImg}
                        alt={character?.name}
                        width={isMobile ? 350 : 278}
                        height={isMobile ? 350 : 278}
                        key={characterImg + new Date().getTime()}
                    />
                    <Col span={8} className="Content">
                        <Row style={{ justifyContent: "space-between" }} className="Title">
                            <Typography variant="h1">{character?.name}</Typography>
                            <FavoriteIcon character={character!} />
                        </Row>
                        <Typography variant="h4" className="Description">
                            {character?.description}
                        </Typography>
                    </Col>
                </CharacterPageWrapperHeader>
                <CharacterPageWrapperContent span={8}>
                    <Typography variant="h2">Comics</Typography>
                    {comics ? (
                        <Carousel gap="16px">{comics?.data.results.map((comic) => <ComicItem key={comic.id} comic={comic} />)}</Carousel>
                    ) : (
                        <Carousel gap="16px">
                            <ComicItem key={"carousel-1"} comic={undefined} />
                            <ComicItem key={"carousel-2"} comic={undefined} />
                        </Carousel>
                    )}
                </CharacterPageWrapperContent>
            </CharacterPageWrapper>
        </CharacterPageRoot>
    );
}

export default CharacterPage;
