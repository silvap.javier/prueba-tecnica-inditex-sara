import styled, { css } from "styled-components";
import BasePage from "../../layout/BasePage/BasePage";
import Col from "../../design-system/Col/Col";


export const HomePageRoot = styled(BasePage)(
    () => css`
    /* Media query para dispositivos móviles */
        @media (max-width: 768px) {
            padding: 0.5rem; /* Elimina el padding en móviles */
        }
    `,
);

export const HomePageWrapper = styled(Col)(
    () => css`
        flex: 1;
    `,
);