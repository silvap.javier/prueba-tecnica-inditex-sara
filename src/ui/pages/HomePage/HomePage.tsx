import clsx from "clsx";
import { HomePageRoot, HomePageWrapper } from "./HomePage.styles";
import { HomePageProps } from "./HomePage.types";
import { useEffect, useState } from "react";
import useGetCharacters from "../../hooks/useGetCharacters";
import CharactersGrid from "../../containers/CharactersGrid/CharactersGrid";
import Filter from "../../containers/Filter/Filter";
import { Result } from "../../types/characters.types";
import { useBaseState } from "../../state/baseState";

function HomePage({ className, ...rest }: HomePageProps): JSX.Element {
    const { data, isLoading } = useGetCharacters();
    const [searhTerm, setSearchTerm] = useState<string>("");
    const [filteredData, setFilteredData] = useState<any>([]);
    const { setIsLoading } = useBaseState();

    useEffect(() => {
        if (data) {
            const filteredData = data?.data.results.filter((character: Result) => {
                return character.name.toLowerCase().includes(searhTerm.toLowerCase());
            });
            setFilteredData(filteredData);
        }
    }, [data, searhTerm]);

    useEffect(() => {
        if (isLoading) {
            setIsLoading(true);
        } else {
            setIsLoading(false);
        }
    }, [isLoading, setIsLoading]);

    return (
        <HomePageRoot className={clsx("home-page", className)} {...rest}>
            <HomePageWrapper span={12}>
                <Filter searhTerm={searhTerm} setSearchTerm={setSearchTerm} count={filteredData.length || 0} />
                <CharactersGrid characters={filteredData} isLoading={isLoading} />
            </HomePageWrapper>
        </HomePageRoot>
    );
}

export default HomePage;
