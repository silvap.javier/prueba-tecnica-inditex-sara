import clsx from "clsx";
import { HomePageRoot, HomePageWrapper } from "./FavoritesPage.styles";
import { HomePageProps } from "./FavoritesPage.types";
import { useBaseState } from "../../state/baseState";
import CharactersGrid from "../../containers/CharactersGrid/CharactersGrid";
import { useEffect, useState } from "react";
import Filter from "../../containers/Filter/Filter";
import { Result } from "../../types/characters.types";

function FavoritesPage({ className, ...rest }: HomePageProps): JSX.Element {
    const { favorites } = useBaseState();

    const [searhTerm, setSearchTerm] = useState<string>("");
    const [filteredData, setFilteredData] = useState<any>([]);

    useEffect(() => {
        if (favorites) {
            const filteredData = favorites?.filter((character: Result) => {
                return character.name.toLowerCase().includes(searhTerm.toLowerCase());
            });

            setFilteredData(filteredData);
        }
    }, [favorites, searhTerm]);

    return (
        <HomePageRoot className={clsx("favorites-page", className)} {...rest}>
            <HomePageWrapper span={12}>
                <Filter searhTerm={searhTerm} setSearchTerm={setSearchTerm} count={filteredData.length || 0} />
                <CharactersGrid characters={filteredData} />
            </HomePageWrapper>
        </HomePageRoot>
    );
}

export default FavoritesPage;
