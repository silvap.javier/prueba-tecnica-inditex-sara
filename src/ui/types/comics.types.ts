export interface ComicsResponse {
    code:            number;
    status:          string;
    copyright:       string;
    attributionText: string;
    attributionHTML: string;
    etag:            string;
    data:            Data;
}

export interface Data {
    offset:  number;
    limit:   number;
    total:   number;
    count:   number;
    results: Result[];
}

export interface Result {
    id:                 number;
    digitalId:          number;
    title:              string;
    issueNumber:        number;
    variantDescription: string;
    description:        string;
    modified:           string;
    isbn:               string;
    upc:                string;
    diamondCode:        string;
    ean:                string;
    issn:               Issn;
    format:             string;
    pageCount:          number;
    textObjects:        TextObject[];
    resourceURI:        string;
    urls:               URL[];
    series:             Series;
    variants:           Series[];
    collections:        any[];
    collectedIssues:    any[];
    dates:              DateElement[];
    prices:             Price[];
    thumbnail:          Thumbnail;
    images:             Thumbnail[];
    creators:           Creators;
    characters:         Characters;
    stories:            Stories;
    events:             Characters;
}

export interface Characters {
    available:     number;
    collectionURI: string;
    items:         Series[];
    returned:      number;
}

export interface Series {
    resourceURI: string;
    name:        string;
}

export interface Creators {
    available:     number;
    collectionURI: string;
    items:         CreatorsItem[];
    returned:      number;
}

export interface CreatorsItem {
    resourceURI: string;
    name:        string;
    role:        string;
}

export interface DateElement {
    type: DateType;
    date: string;
}

export type DateType = "onsaleDate" | "focDate" | "unlimitedDate" | "digitalPurchaseDate";

export interface Thumbnail {
    path:      string;
    extension: Extension;
}

export type Extension = "jpg";

export type Issn = "" | "1938-0380";

export interface Price {
    type:  PriceType;
    price: number;
}

export type PriceType = "printPrice" | "digitalPurchasePrice";

export interface Stories {
    available:     number;
    collectionURI: string;
    items:         StoriesItem[];
    returned:      number;
}

export interface StoriesItem {
    resourceURI: string;
    name:        string;
    type:        ItemType;
}

export type ItemType = "cover" | "interiorStory";

export interface TextObject {
    type:     TextObjectType;
    language: Language;
    text:     string;
}

export type Language = "en-us";

export type TextObjectType = "issue_solicit_text" | "issue_preview_text";

export interface URL {
    type: URLType;
    url:  string;
}

export type URLType = "detail" | "reader" | "purchase" | "inAppLink";