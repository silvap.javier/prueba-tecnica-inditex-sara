import { PropsWithChildren } from "react";
import BaseLayout from "../BaseLayout/BaseLayout";
import Header from "../../navigation/Header/Header";
import Col from "../../design-system/Col/Col";

const MainLayout = ({ children }: PropsWithChildren<{}>): JSX.Element => (
    <BaseLayout>
        <Col gap={"0px"} style={{ padding: 0 }} span={12}>
            <Header />
            {children}
        </Col>
    </BaseLayout>
);

export default MainLayout;
