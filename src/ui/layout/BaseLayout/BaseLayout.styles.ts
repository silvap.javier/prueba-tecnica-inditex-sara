import styled, { css } from "styled-components";

export const BaseLayoutRoot = styled.main(
    () => css`
        flex: 1;
        display: flex;

        @media (max-width: 768px) {
            padding-top: 80px;
        }
    `,
);
