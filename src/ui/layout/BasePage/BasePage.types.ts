import { ReactNode } from "react";

export interface BasePageProps {
    children?: ReactNode;
    className?: string;
    style?: React.CSSProperties;
}