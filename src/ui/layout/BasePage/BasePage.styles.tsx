import styled, { css } from "styled-components";

export const BasePageRoot = styled.div(
    () => css`
        max-width: 100%;
        padding: 25px;
        flex: 1;
        display: flex;
        flex-direction: column;
    `,
);
