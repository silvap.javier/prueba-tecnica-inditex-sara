import { lazy } from "react";
import { Outlet, RouteObject } from "react-router-dom";
import { MainRoutes } from "./MainRouter.types";
import MainLayout from "../layout/MainLayout/MainLayout";

const HomePage = lazy(() => import("../pages/HomePage/HomePage"));
const FavoritesPage = lazy(() => import("../pages/FavoritesPage/FavoritesPage"));
const CharacterPage = lazy(() => import("../pages/CharacterPage/CharacterPage"));

export const useMainRoutes = (): RouteObject[] => {
    return [
        {
            path: MainRoutes.MAIN,
            element: (
                <MainLayout>
                    <Outlet />
                </MainLayout>
            ),
            children: [
                {
                    path: MainRoutes.MAIN,
                    element: <HomePage />,
                },
                {
                    path: MainRoutes.FAVORITES,
                    element: <FavoritesPage />,
                },
                {
                    path: MainRoutes.CHARACTER,
                    element: <CharacterPage />,
                },
            ],
        },
    ];
};
