export enum MainRoutes {
    MAIN = "/",
    FAVORITES = "/favorites",
    CHARACTER = "/character/:id",
}
