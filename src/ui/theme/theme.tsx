// theme.ts

const theme = {
    palette: {
        marvelRed: "#EC1D24",
        black: "#000000",
        white: "#FFFFFF",
        gray: "#AAAAAA",
        lightGray: "#333333",
    },
    fonts: {
        body: "'Roboto Condensed', sans-serif",
    },
    card: {
        width: "188.5px",
        height: "189.97px",
    },
    cardXs: {
        width: "170px",
        height: "170px",
    },
    comic: {
        width: "179px",
        height: "340px",
    },
    borderRadius: "0.5rem",
    spacing: {
        0: "0", // 0px
        1: "0.25rem", // 4px
        1.5: "0.375rem", // 6px
        2: "0.5rem", // 8px
        2.5: "0.625rem", // 10px
        3: "0.75rem", // 12px
        3.5: "0.875rem", // 14px
        4: "1rem", // 16px
        4.5: "1.125rem", // 18px
        5: "1.25rem", // 20px
        6: "1.5rem", // 24px
        7: "1.75rem", // 28px
        8: "2rem", // 32px
        10: "2.5rem", // 40px
        12: "3rem", // 48px
        12.5: "3.125rem", // 50px
        15: "3.75rem", // 60px
        16: "4rem", // 64px
        20: "5rem", // 80px
        22: "5.25rem", // 84px
    },

    breakpoints: {
        xs: "480px",
        sm: "768px",
        md: "1024px",
        lg: "1200px",
    },
};

export default theme;
