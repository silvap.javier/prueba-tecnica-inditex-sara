import React, { FC, ReactNode, CSSProperties } from "react";
import styled from "styled-components";

export interface GridProps extends React.HTMLAttributes<HTMLDivElement> {
    children: ReactNode;
    gap?: string;
    style?: CSSProperties;
}

const StyledGrid = styled.div<{ gap?: string }>`
    display: grid;
    gap: ${(props) => props.gap || "8px"};
    grid-template-columns: repeat(auto-fill, minmax(189px, 1fr)); /* Cambiado auto-fit por auto-fill */
    /* Media query para dispositivos móviles */
    @media (max-width: 768px) {
        grid-template-columns: 190px 2fr;
    }
`;

export const GridItem = styled.div`
    height: 260px;
    margin: auto;
`;

const Grid: FC<GridProps> = ({ children, gap, style, ...rest }) => {
    return (
        <StyledGrid gap={gap} style={style} {...rest}>
            {children}
        </StyledGrid>
    );
};

export default Grid;
