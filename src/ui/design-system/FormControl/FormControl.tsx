import React, { ReactNode, ChangeEvent, KeyboardEvent } from "react";
import styled from "styled-components";
import Row from "../Row/Row";
import searchIcon from "../../assets/images/searchIcon.png";

interface FormControlProps {
    children: ReactNode;
    label?: string;
    className?: string;
}

export const FormControl: React.FC<FormControlProps> = ({ children, label, className }) => {
    return (
        <StyledFormControl className={className}>
            {label && <Label>{label}</Label>}
            {children}
        </StyledFormControl>
    );
};

const StyledFormControl = styled.div`
    margin-bottom: 1rem;
`;

const Label = styled.label`
    display: inline-block;
    margin-bottom: 0.5rem;
    font-weight: 500;
`;

const SearchIcon = styled.img`
    width: 14px;
    height: 14px;
    display: inline-block;
`;

export interface InputProps {
    placeholder?: string;
    value?: string;
    onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
    onKeyPress?: (e: KeyboardEvent<HTMLInputElement>) => void;
    className?: string;
}

export const Input: React.FC<InputProps> = ({ placeholder, value, onChange, onKeyPress, className }) => {
    return (
        <Row style={{ width: "100%", borderBottom: "1px solid #000000" }} gap={"0px"}>
            <SearchIcon src={searchIcon} />
            <StyledInput placeholder={placeholder} value={value} onChange={onChange} onKeyPress={onKeyPress} className={className} />
        </Row>
    );
};

const StyledInput = styled.input`
    display: inline-block;
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    font-weight: 400;
    color: ##aaaaaa;
    background-color: #fff;
    border: transparent;
    border-radius: 0;
    transition:
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out;
    &:focus {
        border-color: #000000;
        outline: 0;
    }
`;
