import { styled } from "styled-components";

export const CarouselContainer = styled.div`
    width: 100%;
    overflow-x: auto;
    scroll-snap-type: x mandatory;
    display: flex;
    -webkit-overflow-scrolling: touch;

    &::-webkit-scrollbar {
        display: none; /* Ocultar scrollbar en navegadores que usan Webkit */
    }
`;

export const CarouselWrapper = styled.div`
    display: flex;
    flex-wrap: nowrap;
`;

export const CarouselItem = styled.div`
    flex: 0 0 auto;
    scroll-snap-align: start;
    width: 100%;
    max-width: 100%;

    @media (min-width: 768px) {
        flex: 0 0 50%;
        max-width: 50%;
    }

    @media (min-width: 1024px) {
        flex: 0 0 33.33%;
        max-width: 33.33%;
    }
`;