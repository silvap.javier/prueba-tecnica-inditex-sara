import { FC, ReactNode, useRef, useEffect, useState } from "react";
import styled from "styled-components";

interface CarouselProps {
    children: ReactNode[];
    gap?: string;
}

const CarouselContainer = styled.div`
    width: 100%;
    overflow-x: scroll;
    display: flex;
    scroll-snap-type: x mandatory;
    -webkit-overflow-scrolling: touch;

    /* &::-webkit-scrollbar {
        display: none;
    } */
    padding-bottom: 20px;
`;

const CarouselWrapper = styled.div<{ gap: string }>`
    display: flex;
    flex-wrap: nowrap;
    gap: ${(props) => props.gap};
    scroll-snap-type: x mandatory;
`;

const CarouselItem = styled.div`
    width: 100%;
`;

const Carousel: FC<CarouselProps> = ({ children, gap = "16px" }) => {
    const carouselRef = useRef<HTMLDivElement>(null);
    const [isDragging, setIsDragging] = useState(false);
    const [startX, setStartX] = useState(0);
    const [scrollLeft, setScrollLeft] = useState(0);

    useEffect(() => {
        if (carouselRef.current) {
            const handleWheel = (event: WheelEvent) => {
                if (event.deltaY !== 0) {
                    event.preventDefault();
                    carouselRef.current!.scrollLeft += event.deltaY;
                }
            };

            carouselRef.current.addEventListener("wheel", handleWheel);
            return () => carouselRef.current?.removeEventListener("wheel", handleWheel);
        }
    }, []);

    const startDragging = (e: React.MouseEvent) => {
        setIsDragging(true);
        setStartX(e.pageX - carouselRef.current!.offsetLeft);
        setScrollLeft(carouselRef.current!.scrollLeft);
    };

    const stopDragging = () => {
        setIsDragging(false);
    };

    const onDragging = (e: React.MouseEvent) => {
        if (!isDragging) return;
        e.preventDefault();
        const x = e.pageX - carouselRef.current!.offsetLeft;
        const walk = (x - startX) * 3; // Velocidad del desplazamiento
        carouselRef.current!.scrollLeft = scrollLeft - walk;
    };

    return (
        <CarouselContainer
            ref={carouselRef}
            onMouseDown={startDragging}
            onMouseLeave={stopDragging}
            onMouseUp={stopDragging}
            onMouseMove={onDragging}
            className="carousel"
        >
            <CarouselWrapper gap={gap}>
                {children.map((child, index) => (
                    <CarouselItem key={index}>{child}</CarouselItem>
                ))}
            </CarouselWrapper>
        </CarouselContainer>
    );
};

export default Carousel;
