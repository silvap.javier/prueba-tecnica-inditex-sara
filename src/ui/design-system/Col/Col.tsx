import { FC, ReactNode, CSSProperties } from "react";
import styled from "styled-components";

interface ColProps {
    children: ReactNode;
    span: number;
    gap?: string;
    style?: CSSProperties;
    className?: string;
}

const getWidth = (span: number) => {
    return span ? `${(span / 12) * 100}%` : "100%";
};

const StyledCol = styled.div<ColProps>`
    display: flex;
    flex-direction: column;
    flex: 0 0 ${(props) => getWidth(props.span)};
    max-width: ${(props) => getWidth(props.span)};
    box-sizing: border-box;

    & > * {
        margin-bottom: ${(props) => props.gap || "15px"};

        &:last-child {
            margin-bottom: 0;
        }
    }
`;

const Col: FC<ColProps> = ({ children, span, gap, style, className }) => {
    return (
        <StyledCol span={span} gap={gap} style={style} className={className}>
            {children}
        </StyledCol>
    );
};

export default Col;
