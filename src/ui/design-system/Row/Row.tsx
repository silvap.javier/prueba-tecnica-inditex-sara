import { FC, ReactNode, CSSProperties } from "react";
import styled from "styled-components";

interface RowProps {
    children: ReactNode;
    gap?: string;
    alignItems?: string;
    justifyContent?: string;
    style?: CSSProperties;
    className?: string;
}

const StyledRow = styled.div<RowProps>`
    display: flex;
    flex-wrap: wrap;
    align-items: ${(props) => props.alignItems || "center"};
    justify-content: ${(props) => props.justifyContent || "flex-start"};
    gap: ${(props) => props.gap || "15px"};
`;

const Row: FC<RowProps> = ({ children, gap, alignItems, justifyContent, style, className }) => {
    return (
        <StyledRow 
            gap={gap} 
            alignItems={alignItems} 
            justifyContent={justifyContent} 
            style={style} 
            className={className}
        >
            {children}
        </StyledRow>
    );
};

export default Row;
