import { css, styled } from "styled-components";
import Row from "../../design-system/Row/Row";

export const CharacterCardImg = styled(Row)(
  ({ theme }) => css`
    width: 188.5px;
    border-bottom: 5.38px solid ${theme.palette.marvelRed};
    & > img {
      width: ${theme.card.width};
      height: ${theme.card.height};
      object-fit: cover;
      @media (max-width: 768px) {
        width: ${theme.cardXs.width};
        height: ${theme.cardXs.height};
      }
    }
    @media (max-width: 768px) {
      width: 100%;
    }
  `
);

export const CharacterCardContent = styled("div")(
  ({ theme }) => css`
    width: 100%;
    padding: ${theme.spacing["4"]} ${theme.spacing["4"]} ${theme.spacing["6"]} ${theme.spacing["4"]};
    background-color: ${theme.palette.black};
    position: relative;
    overflow: hidden;
    color: ${theme.palette.white};
    
    .Content{
      width: 100%;
      justify-content: space-between;
      margin:auto;
      & > svg {
        cursor: pointer;
      }
      .Text {
        width: 70%;
      }
    }
    
    &:before {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 0;
      background-color: ${theme.palette.marvelRed};
      transition: height 0.5s ease-in-out;
      z-index: 1;
    }

    &:after {
      content: '';
      position: absolute;
      bottom: 0;
      right: 0;
      width: 0;
      height: 0;
      border-style: solid;
      border-width: 0 0 10px 10px;
      border-color: transparent transparent white transparent;
      z-index: 2;
    }

    & > * {
      position: relative;
      z-index: 3;
    }
  `
);

export const CharacterCardRoot = styled(Row)(
  ({theme}) => css`
    width: ${theme.card.width};
    height: ${theme.card.height};
    row-gap: 0;
    justify-content: center;

    &:hover ${CharacterCardContent}::before {
      height: 100%;
    }

    @media (max-width: 768px) {
      width: ${theme.cardXs.width};
      height: ${theme.cardXs.height};
    }
  `
);


