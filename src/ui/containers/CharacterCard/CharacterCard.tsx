import { useState } from "react";
import Row from "../../design-system/Row/Row";
import { CharacterCardContent, CharacterCardImg, CharacterCardRoot } from "./CharacterCard.styles";
import { CharacterCardProps } from "./CharacterCard.types";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import Typography from "../../components/Typography/Typography";
import { Link } from "react-router-dom";
import FavoriteIcon from "../FavoriteIcon/FavoriteIcon";
import useIsMobile from "../../hooks/useIsMobile";

const CharacterCard = ({ character }: CharacterCardProps): JSX.Element => {
    const [loading, setLoading] = useState(true);
    const isMobile = useIsMobile();
    const characterImg = character?.thumbnail ? `${character?.thumbnail.path}.${character?.thumbnail.extension}` : "";

    const handleImageLoad = () => {
        setLoading(false);
    };

    return (
        <CharacterCardRoot>
            <Link to={`/character/${character?.id}`}>
                <CharacterCardImg>
                    {loading && <Skeleton width={isMobile ? 170 : 188.5} height={isMobile ? 170 : 189.97} />}
                    <img
                        src={characterImg}
                        alt={character?.name}
                        style={{ display: loading ? "none" : "block" }}
                        onLoad={handleImageLoad}
                    />
                </CharacterCardImg>
            </Link>
            <CharacterCardContent>
                <Row className="Content">
                    <Typography variant="h5" className="Text">
                        {character?.name}
                    </Typography>
                    <FavoriteIcon character={character!} />
                </Row>
            </CharacterCardContent>
        </CharacterCardRoot>
    );
};

export default CharacterCard;
