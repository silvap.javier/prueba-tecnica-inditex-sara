import { Result } from "../../types/characters.types";

export interface CharacterCardProps {
    character: Result | undefined;
}