import { css, styled } from "styled-components";
import { HeartIcon as HeartIconSvg} from "../../assets/icons/HeartIcon";
import Row from "../../design-system/Row/Row";



export const HeartIcon = styled(HeartIconSvg)<{ has: boolean }>(
    ({ theme, has }) => css`
        width: 24px;
        height: 22px;
        color: ${ () => {
            return has ? theme.palette.marvelRed : theme.palette.white
        }};

        &:hover {
            color: ${has ? theme.palette.marvelRed : theme.palette.white};
        }
    `,
);

export const FavoritesRoot = styled(Row)(
    ({theme}) => css`
        cursor: pointer;
        .Title{
            color: ${theme.palette.white};
        }
    `,
);

