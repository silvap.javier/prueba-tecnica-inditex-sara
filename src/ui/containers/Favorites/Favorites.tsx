import { Link } from "react-router-dom";
import Typography from "../../components/Typography/Typography";
import { useBaseState } from "../../state/baseState";
import { FavoritesRoot, HeartIcon } from "./Favorites.styles";

const Favorites = (): JSX.Element => {
    const { favorites } = useBaseState();
    const status = favorites.length > 0 ? true : false;
    return (
        <Link to="/favorites" className="Favorites">
            <FavoritesRoot gap={"8px"}>
                <HeartIcon has={status} className={`Icon ${status ? "active" : "empty"}`} />
                <Typography variant="h4" className="Title">
                    {favorites.length}
                </Typography>
            </FavoritesRoot>
        </Link>
    );
};

export default Favorites;
