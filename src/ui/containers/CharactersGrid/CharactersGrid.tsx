import Grid, { GridItem } from "../../design-system/Grid/Grid";
import CharacterCard from "../CharacterCard/CharacterCard";
import { CharactersGridProps } from "./CharactersGrid.types";

const CharactersGrid = ({ characters, isLoading }: CharactersGridProps): JSX.Element => {
    return (
        <Grid>
            <>
                {!isLoading ? (
                    characters?.map((character) => (
                        <GridItem key={character.id}>
                            <CharacterCard character={character} />
                        </GridItem>
                    ))
                ) : (
                    <>
                        <GridItem key={"id-1"}>
                            <CharacterCard character={undefined} />
                        </GridItem>
                        <GridItem key={"id-2"}>
                            <CharacterCard character={undefined} />
                        </GridItem>
                        <GridItem key={"id-3"}>
                            <CharacterCard character={undefined} />
                        </GridItem>
                        <GridItem key={"id-4"}>
                            <CharacterCard character={undefined} />
                        </GridItem>
                        <GridItem key={"id-5"}>
                            <CharacterCard character={undefined} />
                        </GridItem>
                    </>
                )}
            </>
        </Grid>
    );
};

export default CharactersGrid;
