import { Result } from "../../types/characters.types";

export interface CharactersGridProps {
    characters: Result[] | undefined;
    isLoading?: boolean;
}