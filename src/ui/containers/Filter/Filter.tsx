import clsx from "clsx";
import { FilterProps } from "./Filter.types";
import { FilterRoot, FilterWrapper } from "./Filter.styles";
import SearchBar from "../../components/SearchBar/SearchBar";
import Typography from "../../components/Typography/Typography";

function Filter({
  className,
  searhTerm,
  setSearchTerm,
  count,
}: FilterProps): JSX.Element {
  return (
    <FilterRoot className={clsx("Filter", className)}>
      <FilterWrapper gap={"16px"}>
        <SearchBar
          value={searhTerm}
          onSearch={setSearchTerm}
          placeholder="SEARCH A CHARACTER ..."
        />
        <Typography variant="h5" >
          {`${count} RESULTS`}
        </Typography>
      </FilterWrapper>
    </FilterRoot>
  );
}

export default Filter;
