import { css, styled } from "styled-components";
import Row from "../../design-system/Row/Row";

export const FilterRoot = styled(Row)(
    () => css`
        justify-content: flex-start;
        width: 100%;
        @media (max-width: 768px) {
            padding: 0 8px;
            padding-top: 8px;
            width: auto;
        }
    `,
);

export const FilterWrapper = styled(Row)(
    () => css`
        width: 100%;
        justify-content: space-between;
        align-items: center;
    `,
);