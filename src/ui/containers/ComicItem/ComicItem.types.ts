import { Result } from "../../types/comics.types";


export interface ComicItemProps {
    comic: Result | undefined;
}