import { css, styled } from "styled-components";
import Row from "../../design-system/Row/Row";
import Col from "../../design-system/Col/Col";

export const ComicItemImg = styled(Row)(
    ({ theme }) => css`
      width: 188.5px;
      & > img {
        width: ${theme.comic.width};
        height: ${theme.comic.height};
        object-fit: cover;
        @media (max-width: 768px) {
          width: ${theme.comic.width};
          height: ${theme.comic.height};
        }
      }
    `
);

export const ComicItemRoot = styled(Col)(
    () => css`
      width: 188.5px;
    `
);