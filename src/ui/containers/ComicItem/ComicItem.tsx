import { useState } from "react";
import { ComicItemProps } from "./ComicItem.types";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import { ComicItemImg, ComicItemRoot } from "./ComicItem.styles";
import Typography from "../../components/Typography/Typography";
import moment from "moment";

const ComicItem = ({ comic }: ComicItemProps): JSX.Element => {
    const [loading, setLoading] = useState(true);
    const comicImg = comic?.thumbnail ? `${comic?.thumbnail.path}.${comic?.thumbnail.extension}` : "";

    const handleImageLoad = () => {
        setLoading(false);
    };

    return (
        <ComicItemRoot span={8}>
            <ComicItemImg>
                {loading && <Skeleton width={179} height={340} test-id={"skeleton"} />}
                <img src={comicImg} alt={comic?.title} style={{ display: loading ? "none" : "block" }} onLoad={handleImageLoad} />
            </ComicItemImg>
            <Typography variant="h3">{comic?.title}</Typography>
            <Typography variant="h4">{moment(comic?.dates[0].date).format("YYYY")}</Typography>
        </ComicItemRoot>
    );
};

export default ComicItem;
