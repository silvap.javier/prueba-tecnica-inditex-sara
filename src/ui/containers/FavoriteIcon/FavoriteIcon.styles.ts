import { css, styled } from "styled-components";

export const FavoriteIconRoot = styled("div")(
    () => css`
      cursor: pointer;
    `
);