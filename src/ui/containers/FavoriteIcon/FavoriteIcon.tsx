import { useBaseState } from "../../state/baseState";
import { FavoriteIconProps } from "./FavoriteIcon.types";
import { HeartIcon } from "../Favorites/Favorites.styles";
import { Result } from "../../types/characters.types";
import Skeleton from "react-loading-skeleton";
import { FavoriteIconRoot } from "./FavoriteIcon.styles";

const FavoriteIcon = ({ character }: FavoriteIconProps): JSX.Element => {
    const { handleFavorite, getStatusFavorite } = useBaseState();

    const handlesStatusFavorites = () => {
        handleFavorite(character as Result);
    };
    return (
        <FavoriteIconRoot onClick={handlesStatusFavorites}>
            {character ? (
                <div className={`Heart ${getStatusFavorite(character.id) ? "Active" : "Empty"}`}>
                    <HeartIcon has={getStatusFavorite(character.id)} />
                </div>
            ) : (
                <Skeleton width={20} height={20} />
            )}
        </FavoriteIconRoot>
    );
};

export default FavoriteIcon;
