import { Result } from "../../types/characters.types";

export type FavoriteIconProps = {
    character: Result | undefined;
}