import styled, { css } from "styled-components";

interface TypographyRootProps {
    lines?: number;
}

export const TypographyRoot = styled.div<TypographyRootProps>(
    ({ lines }) => css`
        margin: 0;
        ${lines !== undefined &&
        css`
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: ${lines};
            overflow: hidden;
            text-overflow: ellipsis;
        `}

        &.h1 {
            font-size: 2.5rem;
            font-weight: 700;
        }

        &.h2 {
            font-size: 2rem;
            font-weight: 700;
        }

        &.h3 {
            font-size: 1rem;
            font-weight: 500;
        }

        &.h4 {
            font-size: 1rem;
            font-weight: 400;
        }

        &.h5 {
            margin: 0;
            font-size: 0.75rem;
            font-weight: 400;
        }
    `,
);
