import { useState, useEffect } from "react";
import { CharacterImgProps } from "./CharacterImg.types";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import { Fragment } from "react/jsx-runtime";

function CharacterImg({ url, alt, width, height }: CharacterImgProps): JSX.Element {
    const [loading, setLoading] = useState(true);

    const handleImageLoad = () => {
        setLoading(false);
    };

    useEffect(() => {
        setLoading(true);
    }, [url]);
    return (
        <Fragment>
            {loading && <Skeleton width={width} height={height} />}
            <img
                src={url}
                alt={alt}
                width={width}
                height={height}
                style={{ display: loading ? "none" : "block" }}
                onLoad={handleImageLoad}
            />
        </Fragment>
    );
}

export default CharacterImg;
