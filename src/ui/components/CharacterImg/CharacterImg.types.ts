import { ImgHTMLAttributes } from "react";

export interface CharacterImgProps extends ImgHTMLAttributes<HTMLImageElement> {
    url?: string;
    alt?: string;
    type? : "card" | "cardXS" | "comic";
}