import { InputProps } from "../../design-system/FormControl/FormControl";


export interface SearchBarProps extends Omit<InputProps, "prefix" | "defaultValue"> {
    placeholder?: string;
    onSearch: (value: string) => void;
    defaultValue?: string;
}
