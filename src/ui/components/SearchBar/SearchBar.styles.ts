import { styled } from "styled-components";
import { InputGroup } from "../../design-system/InputGroup/InputGroup";
import Row from "../../design-system/Row/Row";

export const SearchContainer = styled(Row)`
  width: 100%;
  padding: 0;
`;

export const StyledInputGroup = styled(InputGroup)`
  flex: 1;
`;