import * as useGetCharacterModule from "../../ui/hooks/useGetCharacters";
import { UseQueryResult } from "@tanstack/react-query";
import HomePage from "../../ui/pages/HomePage/HomePage";
import { CharactersResponse } from "../../ui/types/characters.types";
import { CharacterResponseApi } from "../mock/CharacterResponse";
import { renderWithProviders } from "../utils/Render";

describe("HomePage", () => {
    let useGetCharactersMock: jest.SpyInstance;

    beforeEach(() => {
        // Mockea la respuesta simulada
        useGetCharactersMock = jest.spyOn(useGetCharacterModule, "default").mockReturnValue({
            data: CharacterResponseApi,
            error: null,
            isError: false,
            isLoading: false,
            isSuccess: true,
            refetch: jest.fn(),
        } as unknown as UseQueryResult<CharactersResponse, Error>);
    });

    afterEach(() => {
        // Restaura la implementación original de los hooks
        useGetCharactersMock.mockRestore();
    });

    test("renders HomePage correctly", async () => {
        renderWithProviders(<HomePage />);
    });

    test("calls useGetPodcasts hook with correct parameters", async () => {
        renderWithProviders(<HomePage />);
        expect(useGetCharactersMock).toBeCalledTimes(2);
    });
});
