import { screen } from "@testing-library/react";
import NotFoundPage from "../../ui/pages/NotFoundPage/NotFoundPage";
import { renderWithProviders } from "../utils/Render";

describe("NotFoundPage", () => {
    test("renders correctly", () => {
        renderWithProviders(<NotFoundPage />);

        expect(screen.getByText("404")).toBeInTheDocument();
        expect(screen.getByText("Not Found")).toBeInTheDocument();
        expect(screen.getByText("The content you are looking for does not exist")).toBeInTheDocument();
        const goBackHomeButton = screen.getByRole("button", { name: "Go back home" });
        expect(goBackHomeButton).toBeInTheDocument();
    });
});
