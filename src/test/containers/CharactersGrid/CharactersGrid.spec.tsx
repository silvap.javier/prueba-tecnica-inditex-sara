import { screen } from "@testing-library/react";
import { CharactersGridProps } from "../../../ui/containers/CharactersGrid/CharactersGrid.types";
import { ChatacterMock } from "../../mock/Character.mock";
import CharactersGrid from "../../../ui/containers/CharactersGrid/CharactersGrid";
import { renderWithProviders } from "../../utils/Render";

describe("CharactersGrid component", () => {
    it("renders a list of characters correctly", () => {
        const props: CharactersGridProps = {
            characters: [ChatacterMock, ChatacterMock],
            isLoading: false,
        };

        renderWithProviders(<CharactersGrid {...props} />);
        expect(screen.getAllByText(ChatacterMock.name)).toHaveLength(2);
    });

    it("renders correctly with no characters", () => {
        const props: CharactersGridProps = {
            characters: [],
            isLoading: false,
        };

        renderWithProviders(<CharactersGrid {...props} />);
        expect(screen.queryByText("Spider-Man")).not.toBeInTheDocument();
        expect(screen.queryByText("Iron Man")).not.toBeInTheDocument();
    });
});
