import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { useBaseState } from "../../../ui/state/baseState";
import Favorites from "../../../ui/containers/Favorites/Favorites";
import ConfigProvider from "../../../ui/theme/ConfigProvider";

jest.mock("../../../ui/state/baseState", () => ({
    useBaseState: jest.fn(),
}));

describe("Favorites component", () => {
    it("renders correctly with no favorites", () => {
        (useBaseState as jest.Mock).mockReturnValue({
            favorites: [],
        });

        render(
            <BrowserRouter>
                <ConfigProvider>
                    <Favorites />
                </ConfigProvider>
            </BrowserRouter>,
        );

        expect(screen.getByText("0")).toBeInTheDocument();
        expect(screen.getByRole("link")).toHaveClass("Favorites");
    });

    it("renders correctly with favorites", () => {
        (useBaseState as jest.Mock).mockReturnValue({
            favorites: [{ id: 1, name: "Favorite Character" }],
        });

        render(
            <BrowserRouter>
                <ConfigProvider>
                    <Favorites />
                </ConfigProvider>
            </BrowserRouter>,
        );

        expect(screen.getByText("1")).toBeInTheDocument();
        expect(screen.getByRole("link")).toHaveClass("Favorites");
    });

    it("navigates to the favorites page when clicked", () => {
        (useBaseState as jest.Mock).mockReturnValue({
            favorites: [{ id: 1, name: "Favorite Character" }],
        });

        render(
            <BrowserRouter>
                <ConfigProvider>
                    <Favorites />
                </ConfigProvider>
            </BrowserRouter>,
        );

        const linkElement = screen.getByRole("link");
        expect(linkElement).toHaveAttribute("href", "/favorites");
    });
});
