import { screen } from "@testing-library/react";
import CharacterCard from "../../../ui/containers/CharacterCard/CharacterCard";
import { ChatacterMock } from "../../mock/Character.mock";
import { renderWithProviders } from "../../utils/Render";

describe("CharacterCard component", () => {
    it("renders with character name correctly", () => {
        renderWithProviders(<CharacterCard character={ChatacterMock} />);
        expect(screen.getByText(ChatacterMock.name)).toBeInTheDocument();
    });
});
