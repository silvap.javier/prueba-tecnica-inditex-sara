import { fireEvent, screen, waitFor } from "@testing-library/react";
import moment from "moment";
import ComicItem from "../../../ui/containers/ComicItem/ComicItem";
import { ComicItemProps } from "../../../ui/containers/ComicItem/ComicItem.types";
import { ComicMock } from "../../mock/Comic.mock";
import { renderWithProviders } from "../../utils/Render";

describe("ComicItem component", () => {
    it("renders comic title and date correctly", () => {
        const props: ComicItemProps = {
            comic: ComicMock,
        };

        renderWithProviders(<ComicItem {...props} />);

        expect(screen.getByText(ComicMock.title)).toBeInTheDocument();
        expect(screen.getByText(moment(ComicMock.dates[0].date).format("YYYY"))).toBeInTheDocument();
    });

    it("renders image after loading", async () => {
        const props: ComicItemProps = {
            comic: ComicMock,
        };

        renderWithProviders(<ComicItem {...props} />);

        const img = screen.getByAltText(ComicMock.title) as HTMLImageElement;

        await waitFor(() => {
            fireEvent.load(img);
        });

        expect(img).toBeInTheDocument();
    });
});
