import { BrowserRouter } from "react-router-dom";
import ConfigProvider from "../../ui/theme/ConfigProvider";
import { BaseStateProvider } from "../../ui/state/baseState";
import { render } from "@testing-library/react";

export const renderWithProviders = (ui: React.ReactElement) => {
    return render(
        <BrowserRouter basename="/">
            <ConfigProvider>
                <BaseStateProvider>{ui}</BaseStateProvider>
            </ConfigProvider>
        </BrowserRouter>,
    );
};
