import { render, fireEvent, waitFor } from "@testing-library/react";
import SearchBar from "../../../ui/components/SearchBar/SearchBar";

describe("SearchBar Component", () => {
    it("renders with placeholder and default value correctly", () => {
        const { getByPlaceholderText } = render(<SearchBar placeholder="Search..." defaultValue="initial value" onSearch={() => {}} />);
        const inputElement = getByPlaceholderText("Search...");
        expect(inputElement).toBeInTheDocument();
        expect(inputElement).toHaveValue("initial value");
    });

    it("calls onSearch after debounce when user types", async () => {
        const onSearchMock = jest.fn();
        const { getByPlaceholderText } = render(<SearchBar placeholder="Search..." onSearch={onSearchMock} />);
        const inputElement = getByPlaceholderText("Search...");

        fireEvent.change(inputElement, { target: { value: "test query" } });

        // Wait for debounce timeout (500ms)
        await waitFor(() => expect(onSearchMock).toHaveBeenCalledWith("test query"), { timeout: 600 });
    });

    it("calls onSearch when user presses Enter", async () => {
        const onSearchMock = jest.fn();
        const { getByPlaceholderText } = render(<SearchBar placeholder="Search..." onSearch={onSearchMock} />);
        const inputElement = getByPlaceholderText("Search...");

        fireEvent.change(inputElement, { target: { value: "query on enter" } });
        fireEvent.keyPress(inputElement, { key: "Enter", code: "Enter", charCode: 13 });

        await waitFor(() => expect(onSearchMock).toHaveBeenCalledWith("query on enter"), { timeout: 600 });
    });
});
