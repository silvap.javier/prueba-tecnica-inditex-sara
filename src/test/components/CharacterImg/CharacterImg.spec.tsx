import { render, waitFor } from "@testing-library/react";
import CharacterImg from "../../../ui/components/CharacterImg/CharacterImg";

describe("CharacterImg component", () => {
    it("renders skeleton while image is loading", () => {
        const props = {
            url: "https://example.com/image.png",
            alt: "Test Image",
            width: 300,
            height: 200,
        };

        render(<CharacterImg {...props} />);
    });

    it("renders image after loading", async () => {
        const props = {
            url: "https://example.com/image.png",
            alt: "Test Image",
            width: 300,
            height: 200,
        };

        const { getByAltText } = render(<CharacterImg {...props} />);
        await waitFor(() => {
            const image = getByAltText(props.alt);
            expect(image).toBeInTheDocument();
        });
    });
});
