import { render } from "@testing-library/react";
import Grid, { GridProps } from "../../../ui/design-system/Grid/Grid";

describe("Grid Component", () => {
    const renderComponent = (props: Partial<GridProps> = {}) => {
        const defaultProps: GridProps = {
            children: <div data-testid="child">Grid Item</div>,
            gap: "8px",
            style: {},
        };
        return render(<Grid {...defaultProps} {...props} />);
    };

    it("renders children correctly", () => {
        const { getByTestId } = renderComponent();
        expect(getByTestId("child")).toBeInTheDocument();
    });

    it("renders with default gap if no gap prop is provided", () => {
        const { container } = renderComponent({ gap: undefined });
        const gridContainer = container.firstChild as HTMLElement;
        expect(gridContainer).toHaveStyle("gap: 8px");
    });

    it("renders with specified gap if gap prop is provided", () => {
        const { container } = renderComponent({ gap: "12px" });
        const gridContainer = container.firstChild as HTMLElement;
        expect(gridContainer).toHaveStyle("gap: 12px");
    });
});
