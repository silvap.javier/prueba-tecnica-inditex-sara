import Typography from "../../../ui/components/Typography/Typography";
import { renderWithProviders } from "../../utils/Render";

describe("Typography component", () => {
    test("renders correctly with children and variant", () => {
        const variant = "h1";
        const className = "custom-class";
        const children = "Test";

        const { getByText } = renderWithProviders(
            <Typography variant={variant} className={className}>
                {children}
            </Typography>,
        );

        // Verifica que el componente se haya renderizado correctamente con los children y el variant
        const typographyElement = getByText(children);
        expect(typographyElement).toBeInTheDocument();
        expect(typographyElement).toHaveClass(variant);
        expect(typographyElement).toHaveClass("typography");
        expect(typographyElement).toHaveClass(className);
    });

    test("renders correctly without className", () => {
        const variant = "h2";
        const children = "Test";

        const { getByText } = renderWithProviders(<Typography variant={variant}>{children}</Typography>);

        // Verifica que el componente se haya renderizado correctamente sin className
        const typographyElement = getByText(children);
        expect(typographyElement).toBeInTheDocument();
        expect(typographyElement).toHaveClass(variant);
        expect(typographyElement).toHaveClass("typography");
    });
});
