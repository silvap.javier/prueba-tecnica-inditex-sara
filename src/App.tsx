import { Suspense } from "react";
import Router from "./ui/router/Router";
import ConfigProvider from "./ui/theme/ConfigProvider";
import { BaseStateProvider } from "./ui/state/baseState";

const App = (): JSX.Element | null => {
    return (
        <Suspense fallback={<></>}>
            <ConfigProvider>
                <BaseStateProvider>
                    <Router />
                </BaseStateProvider>
            </ConfigProvider>
        </Suspense>
    );
};

export default App;
