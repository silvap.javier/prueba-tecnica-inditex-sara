/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  moduleDirectories: ["src", "node_modules"],
  transform: {
      "\\.[jt]sx?$": "ts-jest",
      "node_modules/variables/.+\\.(j|t)sx?$": "ts-jest",
  },
  transformIgnorePatterns: ["node_modules/(?!variables/.*)"],
  coveragePathIgnorePatterns: [
      "!*.d.ts",
      "!*styles.tsx",
      "src/hooks/*",
      "src/router/*",
      "src/utils/*",
      "!*.tsx",
  ],
  moduleNameMapper: {
      "\\.(css|less|sass)$": "identity-obj-proxy",
      '\\.(jpg|jpeg|png|gif|svg)$': '<rootDir>/src/test/mock/fileMock.ts', // Mock para archivos de imágenes
  },
  setupFilesAfterEnv: [
    "<rootDir>/src/setupTests.ts"
  ]
};