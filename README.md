# Prueba Técnica Senior Frontend

Proyecto creado con [Create React App](https://github.com/facebook/create-react-app).

## Live

El proyecto esta desplegado en Digital Ocean : https://urchin-app-cz9no.ondigitalocean.app/

## Técnologias empleadas

Para la implementacion de las solusion se utilizaron las siguientes técnologias :

-   react 18.2.0 - Framework de implementacion de SPA
-   typescript 5.4.5 - Para tipado del código
-   styled-components 6.1.8 - Para customizacion de los components
-   react-router-dom 6.22.3 - Para la navegacion y enrutado
-   @tanstack/react-query 5.29.2 - Para el manejo de las peticiones fetch y cache
-   moment 2.30.1 - Manejo de fechas de los episodios y la duración del mismo
-   prettier 3.2.5 - Formateo y limpieza de codigo
-   eslint 8.50.0 - Analisis del codigo posibles errores
-   jest 29.7.0 - Desarrollo de Unit Tests y Component Tests
-   craco 7.1.0 - Para modificaciones avanzadas en la configuración de Webpack logrando la optimizacion de recursos en diferentes enviroments

## Arquitectura

Dentro de src, trata de implementar un diseño de DDD.

-   Components : son los los elementos mas pequeños de la aplicacion y poseen como máximo un state, y son utilizados en la aplicacion
-   Containers : con los componentes que tienen la logica de y utilizan los components o elementos del design-system
-   design-system : se crearon elementos propios de libreria para reutilizarlos en la aplicacion
-   pages : las diferentes pages de la aplicacion que utilizan los containers
-   theme : una configuracion para el tema de la app
-   Se implemento skeleton en los sitios necesario para una mejor experiencia de usuario

## Scripts

Se crearon los siguientes scrips :

### `yarn start`

Corre la aplicacion de modo development en local : [http://localhost:3000](http://localhost:3000)

### `yarn test`

Lanzar test con jest visualizacion el coverage.

### `yarn lint:fix`

Correccion y deteccion de errores
